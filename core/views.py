#KANG CHRISTINE NGA MGA IMPORTS

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from django.http import JsonResponse
from django.http import Http404
from .models import Account
from .serializers import AccountSerializer

class UserViews(APIView):

    def get(self, request, format = None):
        if 'id' in request.GET:
            try:
                id = request.GET['id']
                Account = Account.objects.get(pk=id)
                serializer = AccountSerializer(Account)
                return Response(serializer.data)
            except Account.DoesNotExist:
                result = "supportOffice Does Not Exist!"
                return(result)
        else:
            Account = Account.objects.all()
            serializer = AccountSerializer(Account, many=True)
            return Response(serializer.data)

    def post(self, request, format=None):
        email = request.data['emal']
        # user_id
        surname = request.data['surname']
        fname = request.data['fname']
        # username
        # password
        
        if not email and surname and fname:
            raise Http404
        else:
            try:
                last_account = Account.objects.latest('id')
            except Account.DoesNotExist:
                account = Account()
                account.email = email
                account.user_id = 1000000
                account.surname = surname
                account.fname = fname
                account.username = '1000000'
                account.password = '1000000'
                account.result = 'First Account Successfully Added'
                account.save()
                serializer = AccountSerializer(account)
                return Response(serializer.data)
            else:
                user_id = last_account.user_id + 1
                account = Account()
                account.email = email
                account.user_id = user_id
                account.surname = surname
                account.fname = fname
                account.username = str(user_id)
                account.password = str(user_id)
                account.result = 'New Account Successfully Added'
                account.save()
                serializer = AccountSerializer(account)
                return Response(serializer.data)

    # getting the object
    def get_object(self, pk):
        try:
            return Account.objects.get(pk=pk)
        except Account.DoesNotExist:
            raise Http404


    # query using GET
    def get(self, request, format=None):
        id = request.GET['id']
        account = self.get_object(id)
        serializer = AccountSerializer(account)
        return Response(serializer.data)

    # edit using PUT
    def put(self, request, format=None):
        id = request.GET['id']
        account = self.get_object(id)

        username = request.data['username']
        password = request.data['password']

        if not username and not password:
            account.result = 'Please input username or password'
        else:
            if username == account.username and password == account.password:
                account.result = 'Error'
            else:
                account.username = request.data['username']
                account.password = request.data['password']
                account.result = 'Successful'
                account.save()

        serializer = AccountSerializer(account)
        # if serializer.is_valid():
        return Response(serializer.data)

# class Account(APIView):
#     # authentication_class = (TokenAuthentication,)
#     # permission_classes = (IsAuthenticated,)

#     def get(self, request, format=None):
#         if 'id' in request.GET:
#             try:
#                 id = request.GET['id']
#                 Account = Account.objects.get(pk=id)
#                 serializer = AccountListSerializer(Account)
#                 return Response(serializer.data)
#             except Account.DoesNotExist:
#                 result = "supportOffice Does Not Exist!"
#                 return(result)
#         else:
#             Account = Account.objects.all()
#             serializer = AccountListSerializer(Account, many=True)
#             return Response(serializer.data)

#     def post(self, request):                
#         serializer = AccountListSerializer1(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         else:
#             result = "Account Already Exist!"
#             return Response(result)

#     def put(self, request):
#         id = request.GET['id']
#         Account = Account.objects.get(pk=id)
#         serializer = AccountEditSerializer(Account, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         else:
#             result = "No Changes"
#             return Response(result)

# class AccountDetails(APIView):

#     def get(self, request, format=None):
#         if 'id' in request.GET:
#             try:
#                 id = request.GET['id']
#                 Account = Account.objects.get(pk=id)
#                 AccountDetails = Account_Details.objects.all()._next_is_sticky().filter(AccountID=Account)
#                 AccountDetails = Account_DetailsListSerializer(AccountDetails, many=True)
#                 return Response(AccountDetails.data)
#             except Account_Details.DoesNotExist:
#                 result = "supportOffice Does Not Exist!"
#                 return(result)
#         else:
#             AccountDetails = Account_Details.objects.all()
#             serializer = Account_DetailsListSerializer(AccountDetails, many=True)
#             return Response(serializer.data)

#     def post(self, request):
#         Accountdetails = Account_DetailsAddSerializer(data=request.data)
#         if Accountdetails.is_valid():
#             Accountdetails.save()
#             return Response(Accountdetails.data)
#         else:
#             result = "Account Details Already Exist!"
#             return Response(result)

#     def put(self, request, format=None):
#         id = request.GET['id']
#         Account_details = Account_Details.objects.get(pk=id)
#         serializer = Account_DetailsEditSerializer(Account_details, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         else:
#             result = "No Changes"
#             return Response(result)
