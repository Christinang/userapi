from rest_framework import serializers
# from rest_framework.serializers import EmailField, ValidationError, CharField
# from django.contrib.contenttypes.models import ContentType
# from django.db.models import Q
# from django.shortcuts import render, HttpResponse
from .models import Account

class AccountSerializer(serializers.ModelSeriailzer):
	result = CharField(allow_blank=True, read_only=True)
	class Meta:
		model = Account
		fields = [
			'id',
			'user_id',
			'email',
			'surname',
			'fname',
			'username',
			'password',
			'result',
		]