from django.apps import AppConfig


class UsersAPIConfig(AppConfig):
    name = 'usersAPI'
