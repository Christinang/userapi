from django.db import models 
from django.conf import settings

from django.conf import settings


class Account(models.Model):
	designation = models.ForeignKey(Designation, on_delete = models.CASCADE, null=True)
	user_id = models.IntegerField(null=True)
	email = models.CharField(max_length=50, null=True)
	surname = models.CharField(max_length=50, null=True)
	fname = models.CharField(max_length=50, null=True)
	username = models.CharField(max_length=50, null=True)
	password = models.CharField(max_length=50, null=True)
	
	def __str__(self):
		return str(self.fname) + ' ' + str(self.surname)